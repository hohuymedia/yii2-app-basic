<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => Yii::$app->name,
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = [];
            foreach (\Yii::$app->getModules() as $name => $m) {
                $module = \Yii::$app->getModule($name);
                        // Automatically add menu items in case of `_menu[ModuleID].php` exists in module or app path...
                        $items = [];
                if ($module && (file_exists($phpFile = $module->getViewPath().'/layouts/_menu'.ucfirst($name).'.php'))) {
                    $items = array_merge_recursive($items, require($phpFile));
                } elseif ($module && (file_exists($phpFile = \Yii::$app->getViewPath().'/layouts/_menu'.ucfirst($name).'.php'))) {
                    $items = array_merge_recursive($items, require($phpFile));
                }

                $autoMenuItems[$name] = [
                    'label' => ucfirst($name),
                    'url' => ['/'.$module->id],
                    'visible' => YII_DEBUG || false,
                ];
                if (count($items)) {
                    foreach ($items as $k => $menuItem){
                      if (!array_key_exists('visible', $menuItem)){
                        $items[$k]['visible'] = YII_DEBUG || Yii::$app->user->can($menuItem['url'][0]);
                      }
                      if ($items[$k]['visible']) $autoMenuItems[$name]['visible'] = true;
                    }
                    $autoMenuItems[$name]['url'] = '#';
                    $autoMenuItems[$name]['items'] = $items;
                }
            }

            $menuItems = ArrayHelper::merge($menuItems, $autoMenuItems);

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => ArrayHelper::merge([
                    ['label' => 'Home', 'url' => ['/site/index']],
                    ['label' => 'About', 'url' => ['/site/about']],
                ], $menuItems, [
                  ['label' => 'Contact', 'url' => ['/site/contact']],
                  Yii::$app->user->isGuest ?
                      ['label' => 'Login', 'url' => Yii::$app->user->loginUrl] :
                      ['label' => 'Logout ('.Yii::$app->user->identity->username.')',
                          'url' => ['/user/security/logout'],
                          'linkOptions' => ['data-method' => 'post']],
                ]),
            ]);
            NavBar::end();
        ?>

        <div class="container">

            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

<?php foreach (Yii::$app->getSession()->getAllFlashes() as $key => $message) {
    if (is_array($message)) {
        $message = implode('<br/>', $message);
    }
    echo Alert::widget(['options' => ['class' => 'alert-'.$key], 'body' => $message]);
}?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; <?= Yii::$app->name ?> <?= date('Y') ?></p>
            <p class="pull-right"><?= date('Y-m-d H:i:s') ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
