<?php
/**
 * This is the template for generating a module class file.
 */

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\module\Generator */

$className = $generator->moduleClass;
$pos = strrpos($className, '\\');
$ns = ltrim(substr($className, 0, $pos), '\\');
$className = substr($className, $pos + 1);

echo "<?php\n";
?>
namespace <?= $ns ?>;

use yii\base\BootstrapInterface;
use yii\web\GroupUrlRule;
use yii\console\Application as ConsoleApplication;
use yii\base\Module;
use Yii;

class Bootstrap implements BootstrapInterface
{
    /** @inheritdoc */
    public function bootstrap($app)
    {
        /** @var $module Module */
        if ($app->hasModule('<?= $generator->moduleID ?>') && ($module = $app->getModule('<?= $generator->moduleID ?>')) instanceof Module) {
            if ($app instanceof ConsoleApplication) {
                $module->controllerNamespace = '<?= $ns ?>\commands';
            } else {
                $configUrlRule = [
                    'prefix' => $module->urlPrefix,
                    'rules'  => $module->urlRules
                ];

                if ($module->urlPrefix != '<?= $generator->moduleID ?>') {
                    $configUrlRule['routePrefix'] = '<?= $generator->moduleID ?>';
                }

                $app->get('urlManager')->rules[] = new GroupUrlRule($configUrlRule);
            }

            $app->get('i18n')->translations['<?= $generator->moduleID ?>*'] = [
                'class'    => 'yii\i18n\PhpMessageSource',
                'basePath' => __DIR__ . '/messages',
            ];
        }
    }
}
