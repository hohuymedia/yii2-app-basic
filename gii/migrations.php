<?php
/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name */
$controller = Yii::$app->controller;
$schema = Yii::$app->get($controller->db)->getSchema();
$tables = explode(",", Yii::$app->controller->migrationTable);
echo "<?php\n";
?>

use yii\db\Migration;
use yii\db\Schema;

class <?= $className ?> extends Migration
{
    /** Table option applied to all tables **/
    public $tableOption;
    /** @TODO: Table definitions goes here **/
    public function init(){
    	if ($this->db->driverName === 'mysql') {
    		$this->tableOption = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
    	}
    }

    /** Upgrade database **/
    public function up()
    {
		/** Create one table
		$this->createTable('{{%table}}', [
			'id' => Schema::TYPE_PK,
			'title' => Schema::TYPE_STRING,
			'description' => Schema::TYPE_TEXT,
			'status' => Schema::TYPE_BOOLEAN,
		], $this->tableOption);
		*/
<?php foreach ($tables as $name): ?> <?php if ($def = $schema->getTableSchema($name)):?>
		/*** Table structure for `<?= $name ?>` ***/
		$this->createTable('{{%<?= $name ?>}}', [
<?php foreach ($def->columns as $col): ?>
			'<?= $col->name ?>' => '<?= $col->type ?>',
<?php endforeach; ?>
		], $this->tableOption);
		$this->addPrimaryKey('pk_<?= $name ?>', '{{%<?= $name ?>}}', '<?= implode(', ', $def->primaryKey) ?>');



<?php endif; ?> <?php endforeach; ?>

	}

    public function down()
    {
<?php foreach ($tables as $name): ?> <?php if ($def = $schema->getTableSchema($name)):?>
		$this->dropTable('{{%<?= $name?>}}');
<?php endif; ?> <?php endforeach; ?>
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
