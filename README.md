# Yii2 Basic Bootstrap Application

Starting point to develope new module for Yii2.

## Installation

```bash
git clone git@bitbucket.org:dinhtrung/yii2-app-basic.git
cd yii2-app-basic
composer install --no-ansi --no-dev --no-interaction --no-progress --no-scripts --optimize-autoloader
composer update

./yii migrate --migrationPath=@vendor/dektrium/yii2-user/migrations
./yii migrate --migrationPath=@vendor/yiisoft/yii2/rbac/migrations
./yii migrate --migrationPath=@vendor/pheme/yii2-settings/migrations
```

## Code Generate from command line

```bash
ns='dinhtrung\assets';
dest='vendor/dinhtrung/yii2-assets-module';
alias='@dinhtrung/assets'
for i in `ls $dest/models/*.php`; do
  j=`basename $i .php`
  ./yii gii/crud --enableI18N=1 --messageCategory='assets' --interactive=0 --overwrite=0 --modelClass=${ns}\\models\\${j} --searchClass=${ns}\\models\\${j}Search --controllerClass=$ns\\controllers\\${j}Controller --viewPath=$alias/views/${j} --template=app
done
```

## Migration Generated from Command line

```bash
# Global migrations
./yii migrate/create --templateFile=@app/gii/migrations.php init

## With table automatically created for you from your DB.
./yii migrate/create --db=[devDb] --migrationTable=[table1,table2,table3] --templateFile=@app/gii/migrations.php --migrationPath=@module/migrations create_table

# Or Module Migrations
../../../yii migrate/create --templateFile=@app/gii/migrations.php --migrationPath=migrations init

```
