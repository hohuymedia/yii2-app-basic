<?php
namespace app\commands;

use yii\helpers\Console;
use yii\helpers\VarDumper;
use Yii;
class TranslateController extends \yii\console\Controller {
	function actionIndex($file, $lang = null){
		if (!$lang) $lang = Yii::$app->language;
		$file = Yii::getAlias($file);
		$trans = Yii::getAlias("@app/trans");
		if (!file_exists($file)) $this->stderr("Cannot find {$file} \n", Console::FG_RED);
		else {
			$data = @require $file;
			foreach ($data as $k => $v){
				if (!$v){
					@exec("$trans -b en:$lang '$k'", $v, $return);
					if (($return == 0) && (is_array($v))){
						$data[$k] = implode("\n", $v);
					}
				}
			}
			print VarDumper::export($data);
		}
	}
}
