<?php
/*
 * ################################################
 * # 1 - Dependency Injection for common widgets #
 * ################################################
 */
\Yii::$container->set ( 'kartik\widgets\DatePicker', [
		'pluginOptions' => [
				'format' => 'yyyy-mm-dd',
				'todayHighlight' => true
		]
] );
\Yii::$container->set ( 'kartik\widgets\TimePicker', [
		'pluginOptions' => [
				'showSeconds' => true,
				'showMeridian' => false
		]
] );
\Yii::$container->set ( 'kartik\widgets\DateTimePicker', [
		'pluginOptions' => [
				'format' => 'yyyy-mm-dd hh:ii:ss',
				'todayHighlight' => true
		]
] );
/*
 * ################################################
 * # 2 - Main web configuration #
 * ################################################
 */

$web = [
		'components' => [
				'request' => [
						'class' => 'yii\web\Request',
						'cookieValidationKey' => 'ebff3d017cc9d3cd73777bae640c96af'
				],
				'user' => [
						'class' => 'app\components\User',
						'identityClass' => 'dektrium\user\models\User',
						'enableAutoLogin' => true,
						'loginUrl' => ['/user/security/login'],
				],
				'errorHandler' => [
						'errorAction' => 'site/error'
				],
/*##########################
# 2b - Custom components #
##########################*/
				'authManager' => 'mdm\admin\components\DbManager',
				'i18n' => [
						'translations' => [
								'*' => [
										'class' => 'yii\i18n\PhpMessageSource',
										'basePath' => '@app/messages'
								]
						]
				],
				'urlManager' => [
						'enablePrettyUrl' => true,
						'showScriptName' => false,
				],
				/*##########################
				# 2c - Module specific components #
				##########################*/
		],
		/*###############
		# 3 - Modules #
		###############*/

		'modules' => [
				'user' => [
						'class' => 'dektrium\user\Module',
						'controllerMap' => [
								'admin' => 'app\controllers\user\AdminController'
						],
				],
				'auth' => 'mdm\admin\Module',
		] ,
		'as access' => [
				'class' => 'mdm\admin\components\AccessControl',
				'allowActions' => [
					'site/*',
					'user/security/*', 'user/recovery/*', 'user/register/*', 'user/settings/*', 'user/registration/*'
				]
		],
];

/*########################
 # 4 - Developement Specific Config       #
 #########################*/
if (YII_DEBUG) {
	// Allow Debug toolbar
	$web ['bootstrap'] [] = 'debug';
	$web ['modules'] ['debug'] = ['class' => 'yii\debug\Module', 'allowedIPs' => ['127.0.0.1', '172.17.0.1']];
	// Allow use GII
	$web ['bootstrap'] [] = 'gii';
	$web ['modules'] ['gii'] = ['class' => 'yii\gii\Module', 'allowedIPs' => ['127.0.0.1', '172.17.0.1']];
	// Allow access all
	$web['as access']['allowActions'][] = '*';
}

return \yii\helpers\ArrayHelper::merge( require(__DIR__ . '/common.php') , $web, require(__DIR__ . '/local/web.php'));
