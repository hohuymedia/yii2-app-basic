<?php
/**
* Global Parameters for our fancy Application
**/
$params = \yii\helpers\ArrayHelper::merge(require (__DIR__ . '/params.php'), require(__DIR__ . '/local/params.php'));
$common = [
		'id' => 'webapp',
		'name' => 'Yii2 Base Application',
		'timeZone' => 'Asia/Ho_Chi_Minh',
		'language' => 'vi',
		'basePath' => dirname ( __DIR__ ),
		'bootstrap' => [ 'log' ],
		'components' => [
				'cache' => [
						'class' => 'yii\caching\FileCache'
				],
				'mailer' => [
						'class' => 'yii\swiftmailer\Mailer',
						'useFileTransport' => YII_DEBUG
				],
				'log' => [
						'traceLevel' => YII_DEBUG ? 3 : 0,
						'targets' => [
								[
										'class' => 'yii\log\FileTarget',
										'levels' => [ 'error', 'warning', 'trace', 'info' ]
								]
						]
				],
				'db' => [
						'class' => 'yii\db\Connection',
						'dsn' => 'mysql:host=db;dbname=webapp',
						'username' => 'webapp',
						'password' => 'pass4webapp',
						'charset' => 'utf8'
				],
/*##########################
 # 2b - Custom components #
##########################*/
				'authManager' => 'yii\rbac\DbManager',
				'i18n' => [
						'translations' => [
								'*' => [
										'class' => 'yii\i18n\PhpMessageSource',
										'basePath' => '@app/messages'
								]
						]
				],
/*##########################
 # 2c - Module specific components #
##########################*/
		],
		'params' => $params,

/*###############
 # 3 - Modules #
###############*/

		'modules' => [
				'user' => 'dektrium\user\Module',
		]
];

/*########################
 # 4 - Developement Specific Config       #
#########################*/

if (YII_DEBUG){
	$common ['bootstrap'] [] = 'gii';
	$common['modules']['gii'] = [
			'class' => 'yii\gii\Module',
			'generators' => [
		        'crud'   => [
		            'class'     => 'yii\gii\generators\crud\Generator',
		            'templates' => [
										'app' => '@app/gii/crud/app'
								]
		        ],
						'module'   => [
		            'class'     => 'app\gii\module\Generator',
		            'templates' => [
										'app' => '@app/gii/module/app'
								]
		        ],
						'model'   => [
		            'class'     => 'yii\gii\generators\model\Generator',
		            'templates' => [
										'app' => '@app/gii/model/app'
								]
		        ],
		    ]
		];
}



return \yii\helpers\ArrayHelper::merge( $common, require(__DIR__ . '/local/common.php'));
